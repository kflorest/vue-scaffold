// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import Vuex from 'vuex';
import App from './App';
import router from './router';
// Global
import globalComponents from './global/globalComponents';
import globalDirectives from './global/globalDirectives';
import globalStore from './global/globalStore';

Vue.config.productionTip = false;

Vue.use(Vuex);
Vue.use(globalComponents);
Vue.use(globalDirectives);
Vue.use(globalStore);
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
});
