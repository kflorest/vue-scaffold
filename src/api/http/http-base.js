/**
 * HTTP Base
 */
// variables
const domain = 'dev.domain';
const path = 'api';
const protocol = 'http';
// Base
const base = () => ({
  url: `${protocol}://${domain}/${path}/`,
});
// Export
export default base;
