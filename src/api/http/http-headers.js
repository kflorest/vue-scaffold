/**
 * HTTP Headers
 */
// Token
const token = () => ({
  get: localStorage.toke,
});
// headers
const headers = () => ({
  getHeaders: {
    Authentication: `bearer ${token.get}`,
  },
});
// Export
export default headers;
