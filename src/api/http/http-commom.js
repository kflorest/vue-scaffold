/**
 * HTTP Common
 */
import axios from 'axios';
import headers from './http-headers';
import base from './http-base';
// Create instance
const instance = () => {
  return axios.create({
    baseURL: base.url,
    headers: headers.getHeaders,
  });
};
// Export
export default instance;
